<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="py-5">
      <div class="row">
        <div class="col-sm-4 mb-4">
          <div class="image image-half d-block text-center"><img src="images/img-1.1.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-4 -->
        <div class="col-sm-8">
          <div class="parapgraph-content">
            <h2>IV THERAPY</h2>
            <h3>AT THE OSTEOPATHIC CENTER</h3>
            <p>Need an extra kick to get through the week, attack all those nasty colds, get peak athletic performance? Our custom IV blends are designed to provide quick, potent, and high-quality nourishment for your specific needs.</p>

            <h2>WHAT IS AN IV?</h2>
            <p>Intravenous Therapy (IV Therapy) is the quickest, safest way to administer vitamins, minerals, and other nutrients to enhance immune function, increase energy levels, and help manage a variety of chronic health problems.</p>
            <p>Nutrients strengthen your body’s defenses. When taken orally, nutrients are absorbed as each body allows, subsequently working to shift them into the bloodstream.An IV infusion delivers 100% of each vitamin, mineral, and nutrient directly into the bloodstream, providing maximum absorption for quick results. At the Osteopathic Center we offer a wide array of customizable IV solutions for your specific health needs.</p>
            <p>Some of the most common conditions treated with IV therapy are:</p>
            <ul>
              <li>Asthma, Sinus Infections, Bacterial and Viral Infections</li>
              <li>Chemical Toxicity, Lyme disease, leaky Gut Syndrome</li>
              <li>Chronic Fatigue, Exhaustion, Depression</li>
              <li>Cold or Seasonal allergies</li>
              <li>COPD, lung disease</li>
              <li>Fibromyalgia, Muscles joint and body aches</li>
              <li>Migraine Headaches, Prevention of Atherosclerosis, UTI</li>
            </ul>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-8 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->

    <div class="pt-4">
      <div class="row">
        <div class="col-sm-6 mb-4">
          <div class="parapgraph-content">
            <h2>THE DIFFERENCE</h2>
            <h3>QUALITY & CONCENTRATION</h3>
            <p>Not all IVs are created equal. Our Therapeutic IV infusions are uniquely designed to provide a stronger immune system and overall restorative health.</p>
            <p>The key to an effective and potent IV infusion depends on the QUALITY and CONCENTRATION of each of the specific ingredients. Years of medical training and research have enabled us to create special blends using the right combination AND concentration of ingredients based on factors such as dosages, interactions, tolerances, and the creation of blends that are not only powerful but work synergistically within your body to effectively and safely restore your health.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-6 -->
        <div class="col-sm-6 mb-4">
          <div class="parapgraph-content">
            <h2>PURE INGREDIENTS</h2>
            <p>ALL Osteopathic Center IVs are prepared with concentrated ingredients and the LEAST amount of preservatives, guaranteed to be in the purest form. ALL blends include</p>
            <ul>
              <li>Magnesium </li>
              <li>Bicarbonate</li>
              <li>Selenium </li>
              <li>Zinc</li>
              <li>Calcium Gluconate</li>
              <li>L-Arginine</li>
              <li>Taurine</li>
              <li>Vitamin C* (except Immunity blend) </li>
              <li>B Complex (B1, B2, B3, B5, B6) </li>
              <li>Extra B1, B5, B6</li>
              <li>B 12 (Methyl and Hydroxo-cobalamin)</li>
            </ul>
            <p>*Our vitamin C is tapioca derived to avoid GMOS and allergens usually found in corn</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-4 -->

    <div class="py-4">
      <div class="row">
        <div class="col-sm-12">
          <div class="parapgraph-content">
            <h2>CUSTOM IV</h2>
            <h3>A PERSONALIZED THERAPY</h3>
            <p>We offer custom-tailored IV therapy, starting with comprehensive nutritional and toxicity testing aimed at designing your personalized infusion by mixing an array of nutrients and, in some cases, a combination of more advanced techniques like ultraviolet blood irradiation and ozone blood therapy. Acute and chronically ill patients may receive the most extraordinary benefits from IV therapy using a concentration of nutrients to support them during severe illness and surgical procedures.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-4 -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>