<?php include 'include/header.php';?>

<main id="page-content">
  <div class="page-banner float-left position-relative w-100">
    <div class="slider">
      <div class="bxslider">
        <div><div class="slide_image" style="background-image: url(images/iv_suite_miami_1-1.jpg);"></div></div>
        <div><div class="slide_image" style="background-image: url(images/iv_suite_miami_2.jpg);"></div></div>
        <div><div class="slide_image" style="background-image: url(images/iv_suite_vitality.jpg);"></div></div>
        <div><div class="slide_image" style="background-image: url(images/rich_fronong_iv_suite.jpg);"></div></div>
      </div>
    </div>
    <!-- /.slider -->
    <div class="home-contents">
      <h2>EXPERIENCE MINDFUL</h2>
      <h1>HEALTH</h1>
      <h2>AND WELLNESS</h2>
      <p>Welcome to the IV suite, a state-of-the-art facility designed to provide the most advanced scientific techniques and treatments to radically improve your health, performance and well-being.</p>
      <p>Whether you are running a marathon, want to detoxify your body or simply increase your energy levels, you can achieve optimal health NATURALLY with our advanced IV formulas.</p>
      <p>&nbsp;Schedule your appointment today!&nbsp;</p>
      <h2><span style="color: #ffff99;"><strong>miami (305) 367-1176</strong></span></h2>
    </div>

    <div class="mian-bxslider-button">
      <img src="images/explorebutton.png" class="img-fluid" alt="image" />
    </div>
  </div>
  <!-- /.page-banner -->
  
  <div class="clearfix"></div>

  <div class="schedule_form py-5">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 mb-1 text-center text-uppercase">
          <h2>schedule an appointment</h2>
        </div>
        <!-- /.col-sm-12 -->
        <div class="col-sm-12">
          <form class="form-inline justify-content-center">
            <input type="text" class="form-control text-center form-control-small mb-2 mr-sm-2" placeholder="Full Name" />
            <input type="text" class="form-control text-center form-control-small mb-2 mr-sm-2" placeholder="Phone" />
            <input type="email" class="form-control text-center form-control-small mb-2 mr-sm-2" placeholder="Email" />
            <input type="text" class="form-control text-center form-control-medium mb-2 mr-sm-2" placeholder="How may we help you?" />
            <select class="form-control mr-sm-2 mb-2">
              <option value="--">Select Location</option>
              <option value="miami">Miami</option>
              <option value="tennessee">Tennessee</option>
            </select>
            <button type="submit" class="btn btn-primary mb-2">Send</button>
          </form>
        </div>
        <!-- /.col-sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.schedule_form py-5 -->

  <section class="content_box d-md-flex position-relative">
    <div class="image_bg position-absolute bg-position-right" style="background-image: url(images/releive-migrane-pain.jpg);"></div>
    <div class="cb_textarea">
      <h2>RELIEVE HEADACHES AND FATIGUE</h2>

      <div class="cb_text_description">
        <p><strong>Do you wake up tired? Struggle with intense fatigue or depend on coffee or other stimulants to make it through the day? Dehydration and nutrient deficiency could be to blame.</strong></p>
        <p>Water is essential for life. Our bodies are composed of 60% water and it is estimated that 75% of Americans are chronically dehydrated. Dehydration causes fatigue, headaches, mental fogginess and irritability among other things. Poor nutrition leads to nutrient deficiency which interferes with your body’s metabolism and cellular function. <strong>Intravenous (IV) therapy is the fastest and easiest way to deliver 100% of the necessary vitamins and nutrients directly into the bloodstream</strong>, rapidly replenishing the water and nutrients to restore your health.</p>
      </div>
      <!-- /.cb_text_description -->
    </div>
    <!-- /.cb_textarea -->
  </section>

  <section class="content_box d-md-flex position-relative">
    <div class="image_bg position-absolute bg-position-left" style="background-image: url(images/gain-clarity-and-focus.jpg);"></div>
    <div class="cb_textarea ml-auto">
      <h2>GAIN CLARITY AND FOCUS</h2>

      <div class="cb_text_description">
        <p><strong>Are you looking for the ultimate brain hack? Improve mental focus, increase productivity at work, and enhance studying by helping your body to function at optimal levels.</strong></p>
        <p>Lift the brain fog and fatigue! IV Therapy offers a natural immune boost to help you achieve optimal mental performance and productivity in demanding jobs and busy schedules. <strong>Guard against illness and infections and revitalize your body to increase your energy naturally.</strong> IVs are a natural way to nurture and support your body’s innate healing by helping it perform optimally—thus improving your livelihood!</p>
      </div>
      <!-- /.cb_text_description -->
    </div>
    <!-- /.cb_textarea -->
  </section>

  <section class="content_box d-md-flex position-relative">
    <div class="image_bg position-absolute bg-position-right" style="background-image: url(images/gut_problems.jpg);"></div>
    <div class="cb_textarea">
      <h2>HEAL YOUR GUT</h2>

      <div class="cb_text_description">
        <p><strong>“All chronic diseases begin in the gut.” – Hippocrates</strong></p>
        <p>Dozens of health conditions have been attributed to the influence of the gut microbes. These include obesity, depression, chronic fatigue, Parkinson’s disease, allergies food sensitivities and cancer. Maintaining a well-balanced intestinal flora is essential for optimal health. Stress, poor diet, bacterial and fungal overgrowth can create imbalances in your body and lead to the development of chronic illness. Intravenous (IV) therapy is a great way to support your body during the detoxification and healing process as it allows the nutrients to “bypass” the digestive system for a quicker shot of vitality.</p>
      </div>
      <!-- /.cb_text_description -->
    </div>
    <!-- /.cb_textarea -->
  </section>

  <section class="content_box d-md-flex position-relative">
    <div class="image_bg position-absolute bg-position-left" style="background-image: url(images/dont-let-the-flu-stop-you.jpg);"></div>
    <div class="cb_textarea ml-auto">
      <h2>GET READY FOR THE FLU & COLD SEASON</h2>

      <div class="cb_text_description">
        <p><strong>Support your body while fighting the flu and reduce the recovery time.</strong></p>
        <p>The flu and common cold can not only make you feel miserable but take vital time out of your life. Stress, poor diet and imbalances in the intestinal flora decrease your natural defenses. You may prevent these by strengthening your defenses (immune system) with intravenous therapies that have been long used in the treatment of viral illnesses.</p>
        <p>Intravenous (IV) therapy can strengthen defenses without the use of heavy medications. Through use of a concentrated combination of vitamins and minerals such as <strong>zinc, vitamin C, magnesium, selenium and B vitamins,</strong> in combination with the advanced immune boosters <strong>UBI and OZONE,</strong> you will get back on your feet in no time!</p>
      </div>
      <!-- /.cb_text_description -->
    </div>
    <!-- /.cb_textarea -->
  </section>

  <section class="content_box d-md-flex position-relative">
    <div class="image_bg position-absolute bg-position-right" style="background-image: url(images/protect-against-pathogens.jpg);"></div>
    <div class="cb_textarea">
      <h2>PROTECT AGAINST PATHOGENS</h2>

      <div class="cb_text_description">
        <p>Whether you are running a marathon or want to have more energy at the gym, you can increase your athletic performance NATURALLY with our advanced IV formulas, developed with a very specific combination of key nutrients and vitamins to help you take your training to the next level.</p>
      </div>
      <!-- /.cb_text_description -->
    </div>
    <!-- /.cb_textarea -->
  </section>

</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>