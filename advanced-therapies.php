<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>PEMF</h2>
          <h6>Pulsed electromagnetic therapy</h6>
          <p>This revolutionary non-invasive treatment uses a pulsating electromagnetic field in varying frequencies and intensities and can help you manage pain associated with various bone-related conditions, injuries, and other forms of chronic pain. <br>PEMF Therapy received FDA clearance in 1982 to promote the healing of bone fractures and is in the process of being cleared for many more uses.</p>
          <p>Watch the video to learn more</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9QzMs3jkVQw?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->

    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>BODY SOUND CHAIR</h2>
          <h6>Relax body and mind</h6>
          <p>BodySound™ technology consists of an amplifier and transducers built into a comfortable delivery platform, which uses layered music to induce synchronized sounds, vibrations and electromagnetic fields, designed to produce profound relaxation, stress reduction and facilitate personal growth. During your sessions you will  enter a deep meditative state.</p>
          <p>Watch the video to learn more</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uOcz2K-z6PM?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->

    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>BIO PHOTON</h2>
          <h6>The most advanced technology</h6>
          <p>Bio Photon 100 Professional uses a powerful wave of light to penetrate skin, tissue, and surrounding nerves to treat soft tissue injuries, reduce pain, promote wound healing, and provide therapy for chronic conditions such as arthritis and failed back syndrome. Before and after infrared images of the treated area show increased blood circulation that allows the body to heal itself without drugs or surgery.</p>
          <p>Watch the video to learn more</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/V3IK3JEBysM?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->

    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>OZONE THERAPY</h2>
          <h6>A breath of fresh air for your health </h6>
          <p>Ozone therapy is a one-of-a-kind technique that is both detoxifying and healing. It promotes the amount of oxygen in the body through use of administering ozone into the body with various methods. This therapy is useful for disinfecting the blood and treating diseases like cardiovascular disease, diabetes, Lyme disease, chronic hepatitis, herpes, macular degeneration, and autoimmune disorders.</p>
          <p>Check the video to learn more</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/El58-k3c7mw?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>