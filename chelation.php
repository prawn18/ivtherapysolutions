<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="py-5">
      <div class="row">
        <div class="col-sm-3">
          <div class="image image-half">
            <img src="images/ivsuite_chelation_2.png" class="img-fluid mb-3" alt="image" />
            <p><strong>Benefits:</strong></p>
            <ul>
              <li>Personalized</li>
              <li>Effective </li>
              <li>Patient based</li>
              <li>Accurate</li>
              <li>Quick resolution</li>
            </ul>
          </div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content parapgraph-content-gray">
            <h2>CHELATION THERAPY</h2>
            <p>Chelation therapy is treatment used in conventional medicine for removing heavy metals (including mercury) from the blood. It involves intravenous injections of a chelating agent, that binds to heavy metals and minerals in the blood so that they can be excreted in the urine . Although chelation was originally used to treat conditions like lead poisoning, chelation therapy is now been studied to protect against heart disease and other major health problems</p>

            <h4>Benefits</h4>
            <p>Heavy metals, like arsenic, lead, mercury, and others, are all around us. They’re in the ground we walk on, in the water we drink, and in the products we use every day. But high levels of most heavy metals can make you sick. Chelation therapy is a very effective way to remove several heavy metals from blood, including:</p>
            <ul>
              <li>lead – particularly harmful to young children</li>
              <li>arsenic – associated with higher rates of skin cancer</li>
              <li>mercury – a poison for the nervous system</li>
              <li>iron – </li>
              <li>copper</li>
              <li>nickel</li>
            </ul>

            <h4>How safe  is chelation therapy?</h4>
            <p>Chelation therapy is been used for decades and its origins can be traced back to the early 1930s. Chelation therapy itself began during World War II when chemists at the University of Oxford searched for an antidote for lewisite, an arsenic-based chemical weapon. The chemists learned that EDTA was particularly effective in treating lead poisoning. In the hands of competent professionals Chelation therapy is one the most effective and safe ways to remove heavy metal toxicity to the body, a proper diagnosis is always required before starting chelation therapy. if you suspect you have been in contact with heavy metal toxins contact us and make an appointment to properly assess your case.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>