<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="pt-5">
      <div class="row">
        <div class="col-sm-12">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/4mso2yeOys0?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
          </div>

          <div class="parapgraph-content mt-4">
            <h1>The Experience</h1>
            <p>Relax and enjoy your treatment in our state-of-the-art IV suite, featuring a contemporary design and a variety of comfortable sitting areas that will enhance the benefits of your IV in the maximum state of comfort and safety. Our highly trained staff of doctors, nurses, and specialists will make your comfort and safety their utmost priority.</p>
            <p>Apart from offering a wide range of breakthrough procedures and techniques, the Osteopathic Center’s newly redesigned, modern facilities are fully prepared and equipped for traditional procedures and tests. We have the latest state-of-the-art equipment for use by our own specialists to perform a range of diagnostics and procedures such as offer anesthesia, collect blood/urine samples, as well as perform x-rays and ultrasounds. All our patients receive IV treatments at our ultra-luxe “IV Suite”, a semi-private lounge designed for soothing and relaxation. Our highly-trained and compassionate staff of doctors, nurses, and specialists make your comfort and relief their utmost priority and while undertaking any of our treatments.</p>
          </div>
          <!-- /.paragraph-content -->
        </div>
        <!-- /.col-sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>