<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>Alexandra B.</h2>
          <h6>Rheumatoid Arthritis</h6>
          <p>Alexandra’s quality of life was being threatened by an increasing pain level due to a recent diagnosis of Rheumatoid Arthritis. She visited the Osteopathic Center and began a regimen of specialized IV treatments which resolved her pain. She also notes a major decrease in stress levels and a renewed outlook on life, due to the newly discovered ability to deal with this disease without dangerous and expensive medications.</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9QzMs3jkVQw?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->

    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>Andrew L</h2>
          <h6>IV Therapy Boost / Energy</h6>
          <p>Andrew was leading an exhausting lifestyle of long work days and night classes, in addition to the desire to stay fit and active in his spare time. He discovered specialized IV therapies through the Osteopathic Center and noted a huge improvement not only in his physical wellness, but also his energy and strength levels.</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uOcz2K-z6PM?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->

    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>Cynthia A.</h2>
          <h6>Sore throat / Immune boost</h6>
          <p>After waking up with a sore throat and the fear she was coming down with a seasonal illness, Cynthia decided to try Ultraviolet Blood Irradiation combined with an IV Therapy Boost, and noticed results immediately. Cynthia works at the clinic and recommends UBI for any acute condition, and high-dose Vitamin C for any condition that could benefit from an immune system boost, ranging from chronic fatigue to a typical upper respiratory infection.</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/V3IK3JEBysM?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->

    <div class="row mt-5">
      <div class="col-sm-6">
        <div class="parapgraph-content parapgraph-content-testimonial pt-3">
          <h2>Marcerena & Edwin</h2>
          <h6>Chelation / Heavy metals</h6>
          <p>Marcerena and Edwin took a test administered by The Osteopathic Clinic and were surprised to find that heavy metal toxicity was a major issue for them. Both major proponents of preventative medicine, they underwent Chelation Therapy as well as IV Boost Therapies for prevention of disease and improvement of overall wellbeing.</p>
        </div>
        <!-- /.parapgraph-content -->
      </div>
      <!-- /.col-sm-6 -->
      <div class="col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/El58-k3c7mw?byline=0&portrait=0&rel=0" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.col-sm-6 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>