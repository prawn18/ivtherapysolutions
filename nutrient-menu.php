<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="pt-5">
      <div class="row">
        <div class="col-sm-4">
          <div class="page-heading pr-sm-2 text-uppercase mb-3 text-sm-right">
            <h2>NUTRIENT <br><span>MENU</span></h2>
          </div>
        </div>
        <!-- /.col-sm-4 -->
        <div class="col-sm-8">
          <p>We customize your IV infusions to meet specific needs! Learn how these ingredients have been historically utilized in effectively treating acute illnesses, boosting energy, and improving the overall health of your body.</p>
        </div>
        <!-- /.col-sm-8 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->

    <div class="pb-4">
      <form class="form-inline tt-red justify-content-center">
        <h5 class="text-uppercase mb-2 mr-sm-4">SEARCH THE MENU</h5>
        <input type="text" class="form-control text-center mb-2 mr-sm-4" placeholder="Type here...">
        <button type="submit" class="btn btn-primary pl-5 pr-5 mb-2">Search</button>
      </form>
    </div>

  </div>
  <!-- /.container -->


<div class="my-3">
  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(242,166,30,1);background: -moz-linear-gradient(left, rgba(242,166,30,1) 45%, rgba(226,173,37,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(242,166,30,1)), color-stop(100%, rgba(226,173,37,1)));background: -webkit-linear-gradient(left, rgba(242,166,30,1) 45%, rgba(226,173,37,1) 100%);background: -o-linear-gradient(left, rgba(242,166,30,1) 45%, rgba(226,173,37,1) 100%);background: -ms-linear-gradient(left, rgba(242,166,30,1) 45%, rgba(226,173,37,1) 100%);background: linear-gradient(to right, rgba(242,166,30,1) 45%, rgba(226,173,37,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#242,166,30', endColorstr='#226,173,37', GradientType=1 );">
        BICARBONATE
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Bicarbonate</strong> has been successfully used to treat a variety of poisons and toxins. It is commonly and primarily administered during cardiac resuscitation to ameliorate acidosis. Bicarb is also known for its useful treatment of refractory cancer pain and diabetic ketoacidosis.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(201,142,43,1);background: -moz-linear-gradient(left, rgba(201,142,43,1) 45%, rgba(223,135,38,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(201,142,43,1)), color-stop(100%, rgba(223,135,38,1)));background: -webkit-linear-gradient(left, rgba(201,142,43,1) 45%, rgba(223,135,38,1) 100%);background: -o-linear-gradient(left, rgba(201,142,43,1) 45%, rgba(223,135,38,1) 100%);background: -ms-linear-gradient(left, rgba(201,142,43,1) 45%, rgba(223,135,38,1) 100%);background: linear-gradient(to right, rgba(201,142,43,1) 45%, rgba(223,135,38,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#201,142,43', endColorstr='#223,135,38', GradientType=1 );">
        BIOTIN
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Biotin, Vitamin B7,</strong> is a water-soluble vitamin. Biotin helps thicken hair, nails and greatly benefits the elasticity of the skin. Biotin is important during pregnancy and breastfeeding as it is a crucial nutrient and is important for embryonic growth.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(158, 125, 52,1);background: -moz-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(183, 146, 58,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(158, 125, 52,1)), color-stop(100%, rgba(183, 146, 58,1)));background: -webkit-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(183, 146, 58,1) 100%);background: -o-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(183, 146, 58,1) 100%);background: -ms-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(183, 146, 58,1) 100%);background: linear-gradient(to right, rgba(158, 125, 52,1) 45%, rgba(183, 146, 58,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#158, 125, 52', endColorstr='#183, 146, 58', GradientType=1 );">
        CA GLUCONATE
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Calcium Gluconate</strong> is the calcium salt of gluconic acid, an oxidation product of glucose. It used in the treatment of black widow spider bites to relieve muscle cramping and as an adjunct in the treatment of rickets, osteomalacia, lead colic and magnesium sulfate overdoses.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(93, 155, 177,1);background: -moz-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(124, 209, 230,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(93, 155, 177,1)), color-stop(100%, rgba(124, 209, 230,1)));background: -webkit-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(124, 209, 230,1) 100%);background: -o-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(124, 209, 230,1) 100%);background: -ms-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(124, 209, 230,1) 100%);background: linear-gradient(to right, rgba(93, 155, 177,1) 45%, rgba(124, 209, 230,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#93, 155, 177', endColorstr='#124, 209, 230', GradientType=1 );">
        FOLIC ACID
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Folic acid,</strong> also known as Folate, is well recognized for its value in the prevention of certain birth defects. Higher levels of folic acid significantly reduce the risk of developing high blood pressure, stroke, and the risk of age-related macular degeneration.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(112, 149, 87,1);background: -moz-linear-gradient(left, rgba(112, 149, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(112, 149, 87,1)), color-stop(100%, rgba(132, 196, 68,1)));background: -webkit-linear-gradient(left, rgba(112, 149, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: -o-linear-gradient(left, rgba(112, 149, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: -ms-linear-gradient(left, rgba(112, 149, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: linear-gradient(to right, rgba(112, 149, 87,1) 45%, rgba(132, 196, 68,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#112, 149, 87', endColorstr='#132, 196, 68', GradientType=1 );">
        GLUTATHIONE
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Glutathione (GSH)</strong> is a potent endogenous antioxidant that protects major organs from oxidant injury. Intravenous GSH can reduce or eliminate tissue damage in patients with liver disease and toxicity, and improve pain-free walking distance in patients with peripheral obstructive arterial disease.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(111, 102, 174,1);background: -moz-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(174, 161, 206,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(111, 102, 174,1)), color-stop(100%, rgba(174, 161, 206,1)));background: -webkit-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(174, 161, 206,1) 100%);background: -o-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(174, 161, 206,1) 100%);background: -ms-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(174, 161, 206,1) 100%);background: linear-gradient(to right, rgba(111, 102, 174,1) 45%, rgba(174, 161, 206,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#111, 102, 174', endColorstr='#174, 161, 206', GradientType=1 );">
        L-ARGININE
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>L-Arginine</strong> is an important amino acid involved with different functions in the body regarding wound healing. It is known for helping the kidneys remove waste products and improving symptoms of clogged arteries and coronary artery disease by dilating and relaxing the arteries and improving blood flow in the body.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(112, 150, 87,1);background: -moz-linear-gradient(left, rgba(112, 150, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(112, 150, 87,1)), color-stop(100%, rgba(132, 196, 68,1)));background: -webkit-linear-gradient(left, rgba(112, 150, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: -o-linear-gradient(left, rgba(112, 150, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: -ms-linear-gradient(left, rgba(112, 150, 87,1) 45%, rgba(132, 196, 68,1) 100%);background: linear-gradient(to right, rgba(112, 150, 87,1) 45%, rgba(132, 196, 68,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#112, 150, 87', endColorstr='#132, 196, 68', GradientType=1 );">
        MAGNESIUM
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Magnesium</strong> has been life saving in the treatment of ventricular and atrial cardiac arrhythmias. IV magnesium has been proven to decrease the incidence of death for patients with acute myocardial infarction and reduce vasospasm in patients with subarachnoid hemorrhage.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(57, 108, 142,1);background: -moz-linear-gradient(left, rgba(57, 108, 142,1) 45%, rgba(111, 177, 194,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(57, 108, 142,1)), color-stop(100%, rgba(111, 177, 194,1)));background: -webkit-linear-gradient(left, rgba(57, 108, 142,1) 45%, rgba(111, 177, 194,1) 100%);background: -o-linear-gradient(left, rgba(57, 108, 142,1) 45%, rgba(111, 177, 194,1) 100%);background: -ms-linear-gradient(left, rgba(57, 108, 142,1) 45%, rgba(111, 177, 194,1) 100%);background: linear-gradient(to right, rgba(57, 108, 142,1) 45%, rgba(111, 177, 194,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#57, 108, 142', endColorstr='#111, 177, 194', GradientType=1 );">
        NAC
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>N-Acetyl Cysteine (NAC)</strong> has been shown to prevent and reverse hepatic damage, improve pulmonary function in patients during off-pump coronary bypass surgery and is well known for its life-saving properties in the treatment of acetaminophen overdosage.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(12, 149, 86,1);background: -moz-linear-gradient(left, rgba(12, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(12, 149, 86,1)), color-stop(100%, rgba(132, 194, 69,1)));background: -webkit-linear-gradient(left, rgba(12, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: -o-linear-gradient(left, rgba(12, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: -ms-linear-gradient(left, rgba(12, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: linear-gradient(to right, rgba(12, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#12, 149, 86', endColorstr='#132, 194, 69', GradientType=1 );">
        SELENIUM
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Selenium</strong> is a trace mineral found naturally in the Earth’s soil. It benefits the body by helping to prevent common forms of cancer, fighting off viruses, defending against heart disease, and slowing down symptoms correlated with asthma and other serious conditions.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(170, 188, 58,1);background: -moz-linear-gradient(left, rgba(170, 188, 58,1) 45%, rgba(226, 173, 36,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(170, 188, 58,1)), color-stop(100%, rgba(226, 173, 36,1)));background: -webkit-linear-gradient(left, rgba(170, 188, 58,1) 45%, rgba(226, 173, 36,1) 100%);background: -o-linear-gradient(left, rgba(170, 188, 58,1) 45%, rgba(226, 173, 36,1) 100%);background: -ms-linear-gradient(left, rgba(170, 188, 58,1) 45%, rgba(226, 173, 36,1) 100%);background: linear-gradient(to right, rgba(170, 188, 58,1) 45%, rgba(226, 173, 36,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#170, 188, 58', endColorstr='#226, 173, 36', GradientType=1 );">
        TAURINE
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Taurine</strong> is an amino acid that supports neurological development, promotes cardiovascular health, insulin sensitivity, electrolyte balance, hearing function, and immune modulation. Taurine protects against heart failure and contains anti-obesity and lipid-lowering capabilities.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(242, 166, 30,1);background: -moz-linear-gradient(left, rgba(242, 166, 30,1) 45%, rgba(225, 173, 36,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(242, 166, 30,1)), color-stop(100%, rgba(225, 173, 36,1)));background: -webkit-linear-gradient(left, rgba(242, 166, 30,1) 45%, rgba(225, 173, 36,1) 100%);background: -o-linear-gradient(left, rgba(242, 166, 30,1) 45%, rgba(225, 173, 36,1) 100%);background: -ms-linear-gradient(left, rgba(242, 166, 30,1) 45%, rgba(225, 173, 36,1) 100%);background: linear-gradient(to right, rgba(242, 166, 30,1) 45%, rgba(225, 173, 36,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#242, 166, 30', endColorstr='#225, 173, 36', GradientType=1 );">
        VITAMIN C
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Vitamin C</strong>, Ascorbic Acid, exhibits potent anti-inflammatory and antioxidant effects. Vitamin C significantly mitigates several types of cancer and is beneficial in the treatment of acute and chronic viral infections, chronic fatigue syndrome, and effective removal of toxins from the body.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(202, 142, 43,1);background: -moz-linear-gradient(left, rgba(202, 142, 43,1) 45%, rgba(223, 135, 38,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(202, 142, 43,1)), color-stop(100%, rgba(223, 135, 38,1)));background: -webkit-linear-gradient(left, rgba(202, 142, 43,1) 45%, rgba(223, 135, 38,1) 100%);background: -o-linear-gradient(left, rgba(202, 142, 43,1) 45%, rgba(223, 135, 38,1) 100%);background: -ms-linear-gradient(left, rgba(202, 142, 43,1) 45%, rgba(223, 135, 38,1) 100%);background: linear-gradient(to right, rgba(202, 142, 43,1) 45%, rgba(223, 135, 38,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#202, 142, 43', endColorstr='#223, 135, 38', GradientType=1 );">
        VITAMIN B1
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Vitamin B1</strong>, Thiamine, is a co-enzyme used by the body to metabolize food for energy while ensuring it maintains proper heart and nerve functions. It is a key player in maintaining a healthy metabolism, preventing nerve damage, and decreasing the risk of Wernicke-Korsakoff syndrome.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(158, 125, 52,1);background: -moz-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(182, 146, 58,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(158, 125, 52,1)), color-stop(100%, rgba(182, 146, 58,1)));background: -webkit-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(182, 146, 58,1) 100%);background: -o-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(182, 146, 58,1) 100%);background: -ms-linear-gradient(left, rgba(158, 125, 52,1) 45%, rgba(182, 146, 58,1) 100%);background: linear-gradient(to right, rgba(158, 125, 52,1) 45%, rgba(182, 146, 58,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#158, 125, 52', endColorstr='#182, 146, 58', GradientType=1 );">
        VITAMIN B2
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Vitamin B2</strong>, Riboflavin, is a vitamin that also acts as an antioxidant within the body. It is responsible for maintaining healthy blood cells, boosting energy levels, facilitating a healthy metabolism, preventing free radical damage, contributing to growth, and protecting skin and eye health.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(93, 155, 177,1);background: -moz-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(125, 208, 229,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(93, 155, 177,1)), color-stop(100%, rgba(125, 208, 229,1)));background: -webkit-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(125, 208, 229,1) 100%);background: -o-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(125, 208, 229,1) 100%);background: -ms-linear-gradient(left, rgba(93, 155, 177,1) 45%, rgba(125, 208, 229,1) 100%);background: linear-gradient(to right, rgba(93, 155, 177,1) 45%, rgba(125, 208, 229,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#93, 155, 177', endColorstr='#125, 208, 229', GradientType=1 );">
        VITAMIN B3
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Vitamin B3</strong>, Niacin, plays a significant role in decreasing heart disease. It helps lower bad cholesterol levels and contributes to the elevation of good cholesterol (HDL) and vitamin B3, reduces the risk of Alzheimer’s disease, cataracts, osteoarthritis, and Type 1 diabetes.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(143, 188, 226,1);background: -moz-linear-gradient(left, rgba(143, 188, 226,1) 45%, rgba(171, 159, 205,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(143, 188, 226,1)), color-stop(100%, rgba(171, 159, 205,1)));background: -webkit-linear-gradient(left, rgba(143, 188, 226,1) 45%, rgba(171, 159, 205,1) 100%);background: -o-linear-gradient(left, rgba(143, 188, 226,1) 45%, rgba(171, 159, 205,1) 100%);background: -ms-linear-gradient(left, rgba(143, 188, 226,1) 45%, rgba(171, 159, 205,1) 100%);background: linear-gradient(to right, rgba(143, 188, 226,1) 45%, rgba(171, 159, 205,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#143, 188, 226', endColorstr='#171, 159, 205', GradientType=1 );">
        VITAMIN B5
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Vitamin B5</strong>, Pantothenic Acid, is critical to the production of red blood cells, maintaining a healthy digestive tract, as well as sex and stress-related hormones produced in the adrenal glands.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(111, 102, 174,1);background: -moz-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(175, 161, 206,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(111, 102, 174,1)), color-stop(100%, rgba(175, 161, 206,1)));background: -webkit-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(175, 161, 206,1) 100%);background: -o-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(175, 161, 206,1) 100%);background: -ms-linear-gradient(left, rgba(111, 102, 174,1) 45%, rgba(175, 161, 206,1) 100%);background: linear-gradient(to right, rgba(111, 102, 174,1) 45%, rgba(175, 161, 206,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#111, 102, 174', endColorstr='#175, 161, 206', GradientType=1 );">
        VITAMIN B6
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Vitamin B-6</strong>, Pyridoxine, has been successfully utilized for the treatment of seizures, acute mushroom poisoning, and elevated homocysteine levels. It also helps the body to maintain a healthy nervous system.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(107, 135, 153,1);background: -moz-linear-gradient(left, rgba(107, 135, 153,1) 45%, rgba(127, 180, 75,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(107, 135, 153,1)), color-stop(100%, rgba(127, 180, 75,1)));background: -webkit-linear-gradient(left, rgba(107, 135, 153,1) 45%, rgba(127, 180, 75,1) 100%);background: -o-linear-gradient(left, rgba(107, 135, 153,1) 45%, rgba(127, 180, 75,1) 100%);background: -ms-linear-gradient(left, rgba(107, 135, 153,1) 45%, rgba(127, 180, 75,1) 100%);background: linear-gradient(to right, rgba(107, 135, 153,1) 45%, rgba(127, 180, 75,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#107, 135, 153', endColorstr='#127, 180, 75', GradientType=1 );">
        VITAMIN B12
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Intravenous B12</strong> has been successfully implemented in the treatment of acute cyanide poisoning. It greatly benefits uremic and diabetic neuropathy in dialysis patients. IV B12 has been used effectively in treating Alzheimer like dementia, peripheral neuropathy, and chronic axonal degeneration.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->

  <div class="row mb-3 no-gutters">
    <div class="col-sm-4">
      <div class="gradient-title text-sm-right" style="background: rgba(112, 149, 86,1);background: -moz-linear-gradient(left, rgba(112, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: -webkit-gradient(left top, right top, color-stop(45%, rgba(112, 149, 86,1)), color-stop(100%, rgba(132, 194, 69,1)));background: -webkit-linear-gradient(left, rgba(112, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: -o-linear-gradient(left, rgba(112, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: -ms-linear-gradient(left, rgba(112, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);background: linear-gradient(to right, rgba(112, 149, 86,1) 45%, rgba(132, 194, 69,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#112, 149, 86', endColorstr='#132, 194, 69', GradientType=1 );">
        ZINC
      </div>
      <!-- /.gradient-title text-sm-right -->
    </div>
    <!-- /.col-sm-4 -->
    <div class="col-sm-8">
      <div class="px-4">
        <p><strong>Zinc</strong> is vital for a healthy immune system, correctly synthesizing DNA, promoting healthy growth during childhood, and healing wounds. It reduces the risk of inflammatory diseases and plays a role in maintaining skin integrity and structure.</p>
      </div>
    </div>
    <!-- /.col-sm-8 -->
  </div>
  <!-- /.row -->
</div>
<!-- /.my-2 -->

</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>