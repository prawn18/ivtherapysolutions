<?php include 'include/header.php';?>

<main id="page-content">
  <div class="py-5 location-page">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 mb-4">
          <h2>LOCATIONS</h2>
          <p>Want to start feeling and living better? <br>Please give us a call or fill the form below and we will be in touch with you shortly.</p>

          <form>
            <div class="form-group row">
              <label for="staticName" class="col-sm-2 col-form-label">Name:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="staticName">
              </div>
            </div>
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label">Email:</label>
              <div class="col-sm-10">
                <input type="email" class="form-control" id="staticEmail">
              </div>
            </div>
            <div class="form-group row">
              <label for="staticPhone" class="col-sm-2 col-form-label">Phone:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="staticPhone">
              </div>
            </div>
            <div class="form-group row">
              <label for="staticPhone" class="col-sm-2 col-form-label">Location:</label>
              <div class="col-sm-10">
                <div class="custom-control custom-radio">
                  <input type="radio" id="location1" name="location" class="custom-control-input">
                  <label class="custom-control-label" for="location1">Miami</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" id="location2" name="location" class="custom-control-input">
                  <label class="custom-control-label" for="location2">Tennessee</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label for="staticPhone" class="col-sm-12 col-form-label">Leave your message for the osteopathic center:</label>
              <div class="col-sm-10">
                <textarea rows="5" class="form-control"></textarea>
              </div>
            </div>
            <button class="btn btn-primary" type="button">Submit</button>
          </form>
        </div>
        <!-- /.col-sm-6 -->
        <div class="col-sm-6 text-center">
          <h5>MIAMI OFFICE</h5>
          <h1>PH (305) 367-1176</h1>

          <p>3915 Biscayne Blvd, Suite 406, Miami, FL 33137 <br>FAX (877) 391-0039</p>
          <p>info@theosteocenter.com</p>

          <a class="google-map-link" href="https://www.google.com/maps/place/3915+Biscayne+Blvd+%23406,+Miami,+FL+33137/@25.8136555,-80.1910695,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9b157c0837eb7:0x1a8d78cd15ac0cb9!8m2!3d25.8136507!4d-80.1888808" target="_blank" rel="noopener noreferrer"><img src="images/map-icon.png">Miami Map</a>
        
          <div class="clearfix py-4"></div>
    
          <h5>TENNESSEE OFFICE</h5>
          <h1>PH (865) 321-1732</h1>

          <p>9000 Executive Park Drive, Suite A210 <br>Knoxville, TN 37923 <br>FAX (865) 321-1733</p>
          <p>info.tn@theosteocenter.com</p>

          <a class="google-map-link" href="https://www.google.com/maps/place/9000+Executive+Park+Dr+a310,+Knoxville,+TN+37923/@35.9218574,-84.0817002,17z/data=!3m1!4b1!4m5!3m4!1s0x885c25155803477d:0x852677802ea19982!8m2!3d35.9218531!4d-84.0795115" target="_blank" rel="noopener noreferrer"><img src="images/map-icon.png">Tennessee Map</a>
        </div>
        <!-- /.col-sm-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.py-5 -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>