<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="py-5">
      <div class="row justify-content-center text-center">
        <div class="col-sm-10">
          <div class="page-title">
            <h2>Specialty IV</h2>
            <p>Whether you are running a marathon or want to have more energy at the gym, you can increase your athletic performance NATURALLY with our advanced IV formulas, developed with a very specific combination of key nutrients and vitamins to help you take your training to the next level.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->


    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_ubi.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-orange">
            <h3>UBI THERAPY</h3>
            <p>Are you suffering from one of those “mystery illness” no doctor has been able to resolve? Try UBI, this proven therapy has 90 years of history, helping those who still suffer after exploring other types of medicine. Step into the world of over 140 published medical studies where UBI Therapy has shown amazing success rates.</p>
            
            <ul>
              <li>Muscle and nerve</li>
              <li>Kidney</li>
              <li>Liver</li>
              <li>White blood cells</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->

    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_ozone.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-blue">
            <h3>OZONE THERAPY</h3>
            <p>Do you know the surprising and healing possibilities of Ozone? This powerful agent provides oxygen for your blood in order to unlock an untapped healing mechanism. It is like a breath of fresh air for your health! Take advantage of this often misunderstood molecule to help treat a myriad of diseases. Ozone can detoxify and clean your body in a truly  powerful way!</p>
            
            <ul>
              <li>Oxygenation</li>
              <li>Anti-inflammation</li>
              <li>Anti-bacterial</li>
              <li>Circulation</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->

    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_glutathion.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-green">
            <h3>GLUTATHIONE</h3>
            <p>The “Mother of all Antioxidants”, Glutathione is truly a super star when it comes to activating all the healing mechanisms within your body. Add this push to any of our blends and enjoy the extra benefits of this powerful health superstar. Our GLUTATHIONE PUSH can be added directly to any IV infusion to enhance the benefits of any of your treatments.</p>
            
            <ul>
              <li>Skin Lightening</li>
              <li>Chronic fatigue</li>
              <li>Healing of tissues</li>
              <li>Liver diseases</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>