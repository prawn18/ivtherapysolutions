<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="py-5">
      <div class="row justify-content-center text-center">
        <div class="col-sm-10">
          <div class="page-title">
            <h2>QUICK SHOTS / INJECTABLES</h2>
            <p>Convenient, cost effective way of replacing vitamins and maintaining optimal health while supporting your body’s natural detoxification processes.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->
  </div>
  <!-- /.container -->

  <div class="py-3">
    <div class="row no-gutters justify-content-between">
      <div class="col-md-5">
        <div class="injection injection-1 text-uppercase">
          <img src="images/iv-pushes-icon-2.png">
          INTRAVENOUS<br> <span>PUSHES</span>
        </div>
      </div>
      <!-- /.col-md-5 -->
      <div class="col-md-5 d-none d-md-block">
        <div class="injection injection-2 text-uppercase">
          <img src="images/im-injections-icon-2.png">
          INTRAMUSCULAR<br> <span>INJECTIONS</span>
        </div>
      </div>
      <!-- /.col-md-5 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.py-3 -->

  <div class="container">
    <div class="py-3">
      <div class="row no-gutters container-wrap-out">
        <div class="col-md-6 list-links text-center">
          <ul>
            <li><a href="#ex01"  data-toggle="modal" data-target="#ex01">B12</a></li>
            <li><a href="#ex02"  data-toggle="modal" data-target="#ex02">B-Complex</a></li>
            <li><a href="#ex03"  data-toggle="modal" data-target="#ex03">Glutathione</a></li>
            <li><a href="#ex04"  data-toggle="modal" data-target="#ex04">N-acetyl cysteine (NAC)</a></li>
            <li><a href="#ex05"  data-toggle="modal" data-target="#ex05">Ranitidine (Zantac)</a></li>
            <li><a href="#ex06"  data-toggle="modal" data-target="#ex06">Ketorolac (Toradol)</a></li>
            <li><a href="#ex07"  data-toggle="modal" data-target="#ex07">Ondansetron (Zofran) </a></li>
          </ul>
        </div>
        <!-- /.col-md-6 -->
        <div class="col-md-6 list-links text-center">
          <div class="injection injection-2 text-uppercase text-left d-md-none d-block">
            <img src="images/im-injections-icon-2.png">
            INTRAMUSCULAR<br> <span>INJECTIONS</span>
          </div>

          <ul>
            <li><a href="#ex08"  data-toggle="modal" data-target="#ex08">B12</a></li>
            <li><a href="#ex09"  data-toggle="modal" data-target="#ex09">B-Complex</a></li>
            <li><a href="#ex10"  data-toggle="modal" data-target="#ex10">Glutathione</a></li>
            <li><a href="#ex11"  data-toggle="modal" data-target="#ex11">NAC</a></li>
            <li><a href="#ex12"  data-toggle="modal" data-target="#ex12">NAD +</a></li>
            <li><a href="#ex13"  data-toggle="modal" data-target="#ex13">Ketorolac (Toradol)</a></li>
            <li><a href="#ex14"  data-toggle="modal" data-target="#ex14">Vitamin D</a></li>
            <li><a href="#ex15"  data-toggle="modal" data-target="#ex15">Ondansetron (Zofran) </a></li>
          </ul>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->
  </div>
  <!-- /.container -->


  <!-- Modal ex01 -->
  <div class="modal fade" id="ex01" tabindex="-1" role="dialog" aria-labelledby="ex01Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>B12</h4>
          <p>Intravenous B12 has been successfully implemented in the treatment of acute cyanide poisoning. It greatly benefits uremic and diabetic neuropathy in dialysis patients. IV B12 has been used effectively in treating Alzheimer dementia, peripheral neuropathy, and chronic axonal degeneration.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex02 -->
  <div class="modal fade" id="ex02" tabindex="-1" role="dialog" aria-labelledby="ex02Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>B-Complex</h4>
          <p>B-Complex is a combination of vitamins B1, B2, B3, B5 and B6+</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex03 -->
  <div class="modal fade" id="ex03" tabindex="-1" role="dialog" aria-labelledby="ex03Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Glutathione</h4>
          <p>Glutathione (GSH) is a potent endogenous antioxidant that protects major organs from oxidant injury. Intravenous GSH can reduce or eliminate tissue damage in patients with liver disease and toxicity, and improve pain-free walking distance in patients with peripheral obstructive arterial disease.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex04 -->
  <div class="modal fade" id="ex04" tabindex="-1" role="dialog" aria-labelledby="ex04Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>N-acetyl cysteine (NAC)</h4>
          <p>N-acetyl cysteine (NAC) is used by the body to build antioxidants. Antioxidants are vitamins, minerals, and other nutrients that protect and repair cells from damage. As a prescription drug, doctors use NAC to treat acetaminophen overdose. It may also help break up mucus in people with some lung diseases, like chronic bronchitis and cystic fibrosis.  As a supplement, NAC may be used to protect the liver. There’s evidence it can help prevent bladder and neurologic damage caused by some drugs.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex05 -->
  <div class="modal fade" id="ex05" tabindex="-1" role="dialog" aria-labelledby="ex05Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Ranitidine (Zantac)</h4>
          <p>Ranitidine (aka: Zantac) is used widely to control acid reflux symptoms but it is also an antihistamine that blocks histamine receptor 2. Administration of ranitidine may provide quick relief of gastrointestinal symptoms and help in the allergic response, especially for urticarial reactions. </p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex06 -->
  <div class="modal fade" id="ex06" tabindex="-1" role="dialog" aria-labelledby="ex06Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Ketorolac (Toradol)</h4>
          <p>One of the strongest nonsteroidal anti inflammatories (NSAIDs) to help with pain and inflammation. Usually reserved for pain not responding to other medications and/or treatments</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex07 -->
  <div class="modal fade" id="ex07" tabindex="-1" role="dialog" aria-labelledby="ex07Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Ondansetron (Zofran) </h4>
          <p>Blocks chemicals in the body that can cause nausea and vomiting. This can be used as a preventive or after you have symptoms </p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex08 -->
  <div class="modal fade" id="ex08" tabindex="-1" role="dialog" aria-labelledby="ex08Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>B12</h4>
          <p>Intravenous B12 has been successfully implemented in the treatment of acute cyanide poisoning. It greatly benefits uremic and diabetic neuropathy in dialysis patients. IV B12 has been used effectively in treating Alzheimer dementia, peripheral neuropathy, and chronic axonal degeneration.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex09 -->
  <div class="modal fade" id="ex09" tabindex="-1" role="dialog" aria-labelledby="ex09Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>B-Complex</h4>
          <p>B-Complex is a combination of vitamins B1, B2, B3, B5 and B6</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex10 -->
  <div class="modal fade" id="ex10" tabindex="-1" role="dialog" aria-labelledby="ex10Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Glutathione</h4>
          <p>Glutathione (GSH) is a potent endogenous antioxidant that protects major organs from oxidant injury. Intravenous GSH can reduce or eliminate tissue damage in patients with liver disease and toxicity, and improve pain-free walking distance in patients with peripheral obstructive arterial disease.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex11 -->
  <div class="modal fade" id="ex11" tabindex="-1" role="dialog" aria-labelledby="ex11Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>NAC</h4>
          <p>NAC has been successfully used to treat a variety of poisons and toxins. It is commonly and primarily administered during cardiac resuscitation to ameliorate acidosis. Bicarb is also known for its useful treatment of refractory cancer pain and diabetic ketoacidosis.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex12 -->
  <div class="modal fade" id="ex12" tabindex="-1" role="dialog" aria-labelledby="ex12Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>NAD +</h4>
          <p>Nicotinamide adenine dinucleotide (NAD+) is a coenzyme that is found in every cell of your body and essential for cellular metabolism. This, in a basic sense, results in energy production and detoxification. The uses for NAD+ have been from Anti-Aging to drug and alcohol addiction. </p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex13 -->
  <div class="modal fade" id="ex13" tabindex="-1" role="dialog" aria-labelledby="ex13Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Ketorolac (Toradol)</h4>
          <p>One of the strongest nonsteroidal anti inflammatories (NSAIDs) to help with pain and inflammation. Usually reserved for pain not responding to other medications and/or treatments</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex14 -->
  <div class="modal fade" id="ex14" tabindex="-1" role="dialog" aria-labelledby="ex14Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Vitamin D</h4>
          <p>Vitamin D is an essential vitamin that helps your body absorb calcium and phosphorus. Deficiency of this vitamin had been linked to osteoporosis, pain, hormone problems, muscle weakness, increased risk of cardiovascular disease, asthma, cancer and depression. It is helpful in diabetes, cancer and many other conditions.</p>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal ex15 -->
  <div class="modal fade" id="ex15" tabindex="-1" role="dialog" aria-labelledby="ex15Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4>Ondansetron (Zofran) </h4>
          <p>Blocks chemicals in the body that can cause nausea and vomiting. This can be used as a preventive or after you have symptoms </p>
        </div>
      </div>
    </div>
  </div>
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>