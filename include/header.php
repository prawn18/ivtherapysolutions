<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">

    <title>IV suite - specialized intravenous IV therapy at The Osteopathic Center</title>
  </head>
  <body>
    <div id="wrapper">
      <header id="header">
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">  
              <svg class="" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false">
                <title>Menu</title>
                <path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10"
                  d="M4 7h22M4 15h22M4 23h22"></path>
              </svg>
            </span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.php">What’s IV Therapy</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Our Blends
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="classic-blends.php">Classic Blends</a>
                  <a class="dropdown-item" href="specialty-iv.php">Specialty IV</a>
                  <a class="dropdown-item" href="chelation.php">Chelation</a>
                  <a class="dropdown-item" href="custom-iv-therapy.php">Custom IV</a>
                  <a class="dropdown-item" href="quick-shots.php">Quick Shots</a>
                  <a class="dropdown-item" href="nutrient-menu.php">Nutrient Menu</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  The Iv Suite
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="the-experience.php">The Experience</a>
                  <a class="dropdown-item" href="the-team.php">The Team</a>
                  <a class="dropdown-item" href="advanced-therapies.php">Advanced Therapies</a>
                  <a class="dropdown-item" href="#">Gallery</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Resources
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="testimonial.php">Testimonials</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="locations.php">Visit Us</a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- /#header -->