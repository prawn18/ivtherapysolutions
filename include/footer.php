<footer id="footer" class="mt-5">
        <div class="footer-content py-4 px-sm-3">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-3 col-sm-12">
                <div class="footer-logo">
                  <a href="https://ivsuitemiami.com">
                        <img src="https://ivsuitemiami.com/wp-content/themes/IvSuiteMiami/images/xlogo-footer.png" scale="0">
                    </a>
                </div>

                <div class="social-icons d-block text-center mt-md-0 mt-2 float-md-right">
                    <a href="https://www.youtube.com" target="_blank">
                        <div class="youtube-hover social-slide"></div>
                    </a>
                    <a href="https://www.instagram.com" target="_blank">
                        <div class="instagram-hover social-slide"></div>
                    </a>
                    <a href="https://plus.google.com" target="_blank">
                        <div class="google-hover social-slide"></div>
                    </a>
                    <a href="https://twitter.com" target="_blank">
                        <div class="twitter-hover social-slide"></div>
                    </a>
                    <a href="https://www.facebook.com" target="_blank">
                        <div class="facebook-hover social-slide"></div>
                    </a>
                </div>
              </div>
              <!-- /.col-md-4 -->
              <div class="col-md-4 col-sm-12 offset-md-1">
                <h3>MIAMI OFFICE</h3>
                <h4>The Osteopathic Center</h4>
                <p>
                  <strong class="address-lable">Address:<br></strong>3915 Biscayne Blvd, Suite 406 <br>Miami, FL 33137<br>
                  <strong>Tel:</strong> <a href="tel:0013053671176">(305) 367-1176</a> &nbsp;
                  <strong>Fax:</strong> (877) 391-0039<br>
                  <strong>Email:</strong> <a href="mailto:info@theosteocenter.com">info@theosteocenter.com</a>
                </p>
              </div>
              <!-- /.col-md-4 -->
              <div class="col-md-4 col-sm-12">
                <h3>TENNESSEE OFFICE</h3>
                <h4>The Osteopathic Center</h4>
                <p>
                  <strong class="address-lable">Address:<br></strong>9000 Executive Park Drive, Suite A210 <br>Knoxville, TN 37923<br>
                  <strong>Tel:</strong> <a href="tel:0018653211732">(865) 321-1732</a> &nbsp;
                  <strong>Fax:</strong> (865) 321-1733<br>
                  <strong>Email:</strong> <a href="mailto:info.tn@theosteocenter.com">info.tn@theosteocenter.com</a>
                </p>
              </div>
              <!-- /.col-md-4 -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.container -->
        </div>
        <!-- /.footer-content -->

        <div class="footer-bottom text-sm-left text-center position-relative">
          <div class="carecredit-btn">
            <a target="_blank" href="https://www.carecredit.com/apply/confirm.html?encm=UDFWb10_B2IDP1I2Wm4BaFVvXz0LaQI9VTRWZ1M8AjE&amp;sitecode=B3CALAdToolkitANCardOther"><img src="https://www.carecredit.com/adtoolkit/assets/pages/library/buttons/280x100/CareCredit_Button_ApplyNow_280x100_h_v1.jpg" scale="0"></a>
          </div>
          <div class="container-fluid">
            <div class="row align-items-center">
              <div class="col-sm-5 text-center">
                <button class="btn btn-block btn-sm-inline text-uppercase btn-skyblue">Book An Appointment</button>
              </div>
              <!-- /.col-sm-5 -->
              <div class="col-sm-7 text-sm-right">
                <p>© 2019 <a href="#">The Osteopathic Center</a> | <a href="#">TOS/Privacy Policy</a></p>
              </div>
              <!-- /.col-sm-7 -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.container -->
        </div>
        <!-- /.footer-bottom -->
      </footer>
      <!-- /#footer -->
    </div>
    <!-- /#wrapper -->
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
      $(function(){
        $('.bxslider').bxSlider({
          mode: 'fade',
          pager: false,
          controls: false
        });
      });
    </script>
  </body>
</html>