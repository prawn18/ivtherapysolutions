<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="py-5">
      <div class="row justify-content-center text-center">
        <div class="col-sm-10">
          <div class="page-title">
            <h2>Classic Blends</h2>
            <p>Whether you are running a marathon or want to have more energy at the gym, you can increase your athletic performance NATURALLY with our advanced IV formulas, developed with a very specific combination of key nutrients and vitamins to help you take your training to the next level.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-12 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->


    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_myers.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-orange">
            <h3>MYERS PLUS</h3>
            <p>The Myers’ Cocktail is named for the late John Myers, a physician who used intravenous injections of nutrients to treat many chronic conditions. Our version of the traditional formula contains the same time-tested ingredients with a concentrated twist. Our MYERS’ COCKTAIL package is formulated with all the essential ingredients to recalibrate your body and contains all the nutrients, amino acids, and vitamins included in all of our blends. It will regulate your system and give you a baseline from which to build a custom package to address your unique deficiencies. Some conditions that benefit from this blend:</p>
            
            <ul>
              <li>Severe Asthma Attacks</li>
              <li>Chronic fatigue syndrome (CFS)</li>
              <li>Fibromyalgia</li>
              <li>Chronic Migraines</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->

    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_energy.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-blue">
            <h3>ENERGY</h3>
            <p>Feeling tired, drained, overwhelmed, or fatigued? Can’t make it through the day without copious amounts of coffee? Balance your energy levels NATURALLY, reset your energy baseline with this powerful boost. Our ENERGY package is formulated with a very specific combination of key nutrients and vitamins. When infused regularly, this formula can generate vitality and energy. Take a look at the KEY BENEFITS:</p>
            
            <ul>
              <li>Promotes heart health</li>
              <li>Help regulate Insulin levels</li>
              <li>Balance electrolyte levels</li>
              <li>Aids the fat burning process</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->

    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_energy.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-green">
            <h3>IMMUNITY</h3>
            <p>Feeling under the weather, fighting a nasty cold, or on the brink of getting one? Help your immune system regain strength NATURALLY, helping to fight infections, viruses, and inflammation so you can recover and feel better more quickly. Our IMMUNITY package is formulated with a very specific combination of key nutrients and vitamins. When taking it consistently, this formula strengthens your immune response. Take a look at the KEY BENEFITS:</p>
            
            <ul>
              <li>Strengthen the Immune function</li>
              <li>Aid with digestive function</li>
              <li>Improves Wound healing</li>
              <li>Beneficial for diabetes control</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->

    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_detox.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-orange">
            <h3>DETOX</h3>
            <p>Hangover? Along with bad eating habits, your body’s response to stress could leave you feeling lousy for several days, way long after the party is over. Detoxify your body to aid in the process of removing toxins and rebalancing your system NATURALLY. Our DETOX package is formulated with a very specific combination of key nutrients and vitamins to enhance your life. Benefits your body’s:</p>
            
            <ul>
              <li>Muscles and nerves</li>
              <li>Kidneys</li>
              <li>Liver</li>
              <li>White blood cells</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->

    <div class="py-3">
      <div class="row">
        <div class="col-sm-3 mb-4">
          <div class="image image-half d-block text-center"><img src="images/ivsuite_performance.png" class="img-fluid" alt="image" /></div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content tt-blue">
            <h3>PERFORMANCE</h3>
            <p>Make the most out of your workouts! Whether you are running a marathon or want to have more energy at the gym, you can increase your athletic performance NATURALLY with this advanced formula. Our PERFORMANCE package is formulated with a very specific combination of key nutrients and vitamins. Some of the key benefits:</p>
            
            <ul>
              <li>Increase muscle growth</li>
              <li>Boost endurance</li>
              <li>Promote better blood flow</li>
              <li>Proper circulation</li>
            </ul>

            <a href="#" class="btn btn-primary text-uppercase">learn more</a>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-3 -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>