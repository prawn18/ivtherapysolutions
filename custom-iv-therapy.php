<?php include 'include/header.php';?>

<main id="page-content">
  <div class="container">
    <div class="py-5">
      <div class="row">
        <div class="col-sm-3">
          <div class="image image-half">
            <img src="images/ivsuite_custom_2.png" class="img-fluid mb-3" alt="image" />
            <p><strong>Benefits:</strong></p>
            <ul>
              <li>Personalized</li>
              <li>Effective </li>
              <li>Patient based</li>
              <li>Accurate</li>
              <li>Quick resolution</li>
            </ul>
          </div>
        </div>
        <!-- /.col-sm-3 -->
        <div class="col-sm-9">
          <div class="parapgraph-content parapgraph-content-gray">
            <h2>CUSTOM IV THERAPY</h2>
            <p>The Osteopathic Center offers comprehensive, patient-tailored IV therapy. We begin with nutritional and toxicity testing that helps us understand the state of your health and the particular areas to treat. We then design your custom-tailored infusion mixing an array of nutrients, vitamins, minerals, and in some cases – a combination of more advanced techniques like ultraviolet blood irradiation and ozone therapy, tackling your unique health needs, successfully treating a wide range of symptoms, and aiding in the recovery of many illnesses.</p>

            <h4>Benefits</h4>
            <p>IV nutrients can also be preventive, by strengthening your body’s defenses; as well as anti-aging, by promoting great skin, hair, and nails – helping you look and feel younger. IV therapy is also an excellent tool to prepare for surgery, it decreases the rate of infections, complications, scar-formation and hospitalization.</p>
            <p>Some of the most common conditions treated with IV therapy are:</p>

            <ul>
              <li>Asthma, Sinus Infections, Bacterial and Viral Infections, UTI</li>
              <li>Chemical Toxicity, Lyme disease, leaky Gut Syndrome</li>
              <li>Chronic Fatigue, Exhaustion, Depression</li>
              <li>Cold or Seasonal allergies</li>
              <li>COPD, lung disease</li>
              <li>Fibromyalgia, Muscles joint and body aches</li>
              <li>Migraine Headaches, insomnia </li>
              <li>Parkinson’s disease</li>
              <li>Diabetes </li>
              <li>Prevention of Atherosclerosis</li>
              <li>Cancer </li>
            </ul>

            <h4>How it works?</h4>
            <p>Our Therapeutic IV blends are uniquely designed to provide a stronger immune system and overall restorative health. We offer custom blends designed to maximize your performance, increase your energy levels, strengthen your immune system, and detoxify your body. We also offer the popular “Myers Cocktail” (with our own twist!) The vitamins and minerals injected in each IV blend supply essential nutrients for normal body metabolism and balance restoration.</p>
          </div>
          <!-- /.parapgraph-content -->
        </div>
        <!-- /.col-sm-9 -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.py-5 -->
  </div>
  <!-- /.container -->
</main>
<!-- /#page-content -->

<?php include 'include/footer.php';?>